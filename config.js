let config = {}
// Book Reads Contract Details
config.contractAddress = "0xeAa6F69b423fE02095a28D9A4AEcfAA8966986f4";
config.contractABI = [
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "_val",
				"type": "uint256"
			}
		],
		"name": "updateVal",
		"outputs": [],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [],
		"name": "readVal",
		"outputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			}
		],
		"stateMutability": "view",
		"type": "function"
	}
];
config.activeNetworkId = "4";

// Biconomy configurations
config.biconomy = {
    // dappId : "5d8a04b9c4e711650caf4468",
    apiKey: "TPPc2vqaC.0f82af9f-cec2-4e43-b0d2-404076af48f7"
}

// web3 configurations
config.WEB3_PROVIDER_URL = 'https://rinkeby.infura.io/v3/ead189945a3f4de693665f63c5f1cf4f';


// Address with balance
// config.publicKey = "0x63591A2cA1da598b263c9ee411037972dd063Bd1";
// config.privateKey = "e2031cd4ac15437e0ad16a81abc3510eb1214e6b1f5eb1d8aa8fc19a04fb0b34";

// Address without balance
config.publicKey = "0xAbFe96009c70C1fb1c28b1C2539cD230d83eE887";
config.privateKey = "1d5168adc565364ffcd258f548691bec7ded193080ea5350ad944282bbd2873a";
module.exports = {config}